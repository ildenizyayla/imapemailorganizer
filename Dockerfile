FROM microsoft/dotnet:2.1.0-runtime AS base
WORKDIR /app

FROM microsoft/dotnet:2.1.300-sdk AS build
WORKDIR /src
COPY ["ImapHelper/ImapHelper.csproj", "ImapHelper/"]
RUN dotnet restore ImapHelper/ImapHelper.csproj
COPY . .
WORKDIR /src/ImapHelper
RUN dotnet build ImapHelper.csproj -c Release -o /app

FROM build AS publish
RUN dotnet publish ImapHelper.csproj -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "ImapHelper.dll"]
