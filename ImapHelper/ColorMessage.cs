﻿namespace ImapHelper
{
    using System;
    using System.Drawing;
    using System.Linq;
    using System.Threading.Tasks;
    using ColorConsole = Colorful.Console;

    public static class ColorMessage
    {
        private const string LONG_SEPERATOR = "--------------------------------------------------";
        private const string SHORT_SEPERATOR = "----------";
        private const string EX_MESSAGE = "An exception has occurred.";

        public static async Task ConsoleWrite(string message, bool applyGradient = false)
        {
            if (applyGradient)
                await Task.Run(() => ColorConsole.WriteWithGradient(message.Split(" ").Select(x => $"{x} ").Concat(new[] { "\n" }), Color.Yellow, Color.Green));
            else
                await Task.Run(() => ColorConsole.WriteLine(message, Color.Red));
        }

        public static async Task ConsoleWrite(string message, Color color) =>
            await Task.Run(() => ColorConsole.WriteLine(message, color));

        public static async Task ConsoleWrite(string message, Exception ex)
        {
            await Task.Run(() =>
            {
                ColorConsole.WriteLine(LONG_SEPERATOR, Color.DarkRed);
                ColorConsole.WriteLine(EX_MESSAGE, Color.Red);
                if (!string.IsNullOrEmpty(message))
                    ColorConsole.WriteLine(message, Color.DarkRed);
                ColorConsole.WriteLine(SHORT_SEPERATOR, Color.DarkRed);
                ColorConsole.WriteLine(ex.Message, Color.DarkRed);
                ColorConsole.WriteLine(ex.InnerException == null ? null : ex.InnerException.Message, Color.DarkRed);
                ColorConsole.WriteLine(SHORT_SEPERATOR, Color.DarkRed);
                ColorConsole.WriteLine(LONG_SEPERATOR, Color.DarkRed);
            });
        }

        public static async Task ConsoleWrite(string[] messages) =>
            await Task.Run(() =>
            {
                ColorConsole.WriteLine(LONG_SEPERATOR, Color.DarkRed);
                int r = 255, g = 204, b = 102;
                foreach (var message in messages)
                {
                    ColorConsole.WriteLine(message, Color.FromArgb(r, g, b));
                    r -= 25;
                    b -= 15;
                }
                ColorConsole.WriteLine(LONG_SEPERATOR, Color.DarkRed);
            });

        public static async Task ConsoleHeading(string message) =>
            await Task.Run(() => ColorConsole.WriteAscii(message, Color.FromArgb(244, 212, 255)));

        public static async Task ConsoleRead() =>
            await Task.Run(() => ColorConsole.ReadKey());

    }
}