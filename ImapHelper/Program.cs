﻿namespace ImapHelper
{
    using MailKit;
    using MailKit.Net.Imap;
    using MailKit.Search;
    using Newtonsoft.Json;
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    class Program
    {
        private static ConfigObj APP_CONFIG;
        private static ImapClient CLIENT_IMAP;

        public static void Main(string[] args)
        {
            ColorMessage.ConsoleHeading("WELCOME").Wait();
            APP_CONFIG = BuildConfig().Result;
            CLIENT_IMAP = new ImapClient();
            ProcessTry().Wait();
            ColorMessage.ConsoleWrite("Completed! Press any key to quit!", true).Wait();
            Console.ReadLine();
        }

        private static async Task<bool> ProcessTry()
        {
            var tryCount = 0;

            while (tryCount <= APP_CONFIG.MailRule.MaxTryLimit)
            {
                try
                {
                    if (await ValidateEmailSettings())
                        break;
                }
                catch (Exception ex)
                {
                    tryCount++;
                    await ColorMessage.ConsoleWrite(
                        $"Email settings validation has failed. Try No: {tryCount}." +
                        $"{(tryCount <= APP_CONFIG.MailRule.MaxTryLimit ? " Will be retried." : null)}", true);
                    await ColorMessage.ConsoleWrite(null, ex);
                    await Task.Run(() => System.Threading.Thread.Sleep(APP_CONFIG.MailRule.TryIntervalMS));
                }
            }

            if (tryCount > APP_CONFIG.MailRule.MaxTryLimit)
            {
                await ColorMessage.ConsoleWrite("Email settings validation has failed. Sorry!", true);
                return false;
            }

            await OrganizeEmails();
            return true;
        }

        private static async Task<bool> ValidateEmailSettings()
        {
            await CLIENT_IMAP.ConnectAsync(APP_CONFIG.MailServer.Host, APP_CONFIG.MailServer.Port, APP_CONFIG.MailServer.UseSSL);
            CLIENT_IMAP.AuthenticationMechanisms.Remove("XOAUTH2");
            await CLIENT_IMAP.AuthenticateAsync(APP_CONFIG.MailServer.UserName, APP_CONFIG.MailServer.Password);
            var rootMailFolder = await CLIENT_IMAP.GetFolderAsync(CLIENT_IMAP.PersonalNamespaces[0].Path);

            var folders = APP_CONFIG.MailProcessing.Select(x => x.TargetFolder);

            foreach (var folder in folders)
                if (await rootMailFolder.GetSubfolderAsync(folder) == null)
                    return false;

            return true;
        }

        private static async Task ConnectIMAP(bool disconnect = false)
        {
            if (!CLIENT_IMAP.IsConnected)
            {
                await CLIENT_IMAP.ConnectAsync(APP_CONFIG.MailServer.Host, APP_CONFIG.MailServer.Port, APP_CONFIG.MailServer.UseSSL);
                CLIENT_IMAP.AuthenticationMechanisms.Remove("XOAUTH2");
                await CLIENT_IMAP.AuthenticateAsync(APP_CONFIG.MailServer.UserName, APP_CONFIG.MailServer.Password);
            }

            if (disconnect)
                await DisconnectIMAP();
        }

        private static async Task DisconnectIMAP()
        {
            if (CLIENT_IMAP.IsConnected)
                await CLIENT_IMAP.DisconnectAsync(true);
        }

        private static async Task OrganizeEmails()
        {
            await ConnectIMAP();

            var inbox = CLIENT_IMAP.Inbox;

            foreach (var item in APP_CONFIG.MailProcessing)
            {
                await OrganizeEmail(inbox, item);
                await ColorMessage.ConsoleWrite($"{item.Name} folder's search was completed.", System.Drawing.Color.Crimson);
            }

            await DisconnectIMAP();
        }

        private static async Task OrganizeEmail(IMailFolder inbox, MailProcessingObj processProperty)
        {
            
            var mailQuery = SearchQuery.DeliveredAfter(DateTime.Today.AddDays(-1 * processProperty.SearchDateDaysBeforeToday).ToUniversalTime());
            var targetFolder = CLIENT_IMAP.GetFolder(CLIENT_IMAP.PersonalNamespaces[0]).GetSubfolder(processProperty.TargetFolder);
            inbox.Open(FolderAccess.ReadWrite);

            foreach (var uid in inbox.Search(mailQuery))
            {
                var message = inbox.GetMessage(uid);
                var subject = message.Subject;
                var fromEmail = string.Empty;

                if (message.From.Count > 0)
                {
                    foreach (var frm in message.From.Mailboxes)
                        fromEmail += string.Format("{0};", frm.Address);

                    fromEmail = fromEmail.Length > 1 ? fromEmail.Substring(0, fromEmail.Length - 1) : fromEmail;
                }

                if (processProperty.EmailSenders.Contains(fromEmail, StringComparer.InvariantCultureIgnoreCase))
                    await MoveEmailToAFolder(uid, inbox, targetFolder, subject, fromEmail, processProperty.MarkEmailsAsRead);
            }
        }

        private static async Task MoveEmailToAFolder(UniqueId uid, IMailFolder inbox, IMailFolder targetFolder,
            string subject, string fromEmail, bool markAsRead)
        {
            await ColorMessage.ConsoleWrite(new[] {
                        $"Moving email to {targetFolder}",
                        $"Subject: {new string(subject.Take(255).ToArray())}",
                        $"From: {new string(fromEmail.Take(25).ToArray())}"
                    });

            if (markAsRead)
                await inbox.AddFlagsAsync(uid, MessageFlags.Seen, true);
            await inbox.MoveToAsync(uid, targetFolder);
        }

        private static async Task<ConfigObj> BuildConfig() =>
            JsonConvert.DeserializeObject<ConfigObj>(await File.ReadAllTextAsync(
                Path.Combine(Directory.GetCurrentDirectory(), "appSettings.json")
                ));
    }
}