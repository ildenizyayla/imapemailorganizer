﻿namespace ImapHelper
{
    public class ConfigObj
    {
        public MailServerObj MailServer { get; set; }
        public MailProcessingRules MailRule { get; set; }
        public MailProcessingObj[] MailProcessing { get; set; }
    }

    public class MailServerObj
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public bool UseSSL { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class MailProcessingRules
    {
        public int MaxTryLimit { get; set; }
        public int TryIntervalMS { get; set; }
    }

    public class MailProcessingObj
    {
        public string Name { get; set; }
        public bool MarkEmailsAsRead { get; set; }
        public string TargetFolder { get; set; }
        public int SearchDateDaysBeforeToday { get; set; }
        public string[] EmailSenders { get; set; }
    }
}
