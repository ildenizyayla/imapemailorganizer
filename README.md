# Imap Email Organizer
The application re-organizes inbox folder by moving emails from pre-specified senders at pre-defined date range into pre-specified email folder/label.

For development fun, [Colorful.Console](https://github.com/tomakita/Colorful.Console "Go") is being used on console outputs.

## Getting Started

### Prerequisites
.Net Core runtime is required. Target email folder should have been created and IMAP should have been enabled for the email account.

Account credentials and environment variables must be set by modifying 'appSettings.json' file.

### Installation
The application does not require any pre-installation, other than a internet connection to restore nuget packages.

## Tests
As of yet, no tests were added.

## Deployment
Docker support for Windows OS added. In order to build the image:

```sh
docker build --rm -f "Dockerfile" -t imapemailorganizer:latest .
```

In order to run the image:

```sh
docker run --rm -it imapemailorganizer:latest
```

## License
This project is not licensed.